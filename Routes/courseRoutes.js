const express = require('express');
const router = express.Router();
const courseController = require('../Controllers/courseControllers.js');
const auth = require('../auth.js');

// Route for creating a course
router.post("/addCourse", auth.verify, courseController.addCourse);
// Route for retrieving all courses
router.get('/allCourses', auth.verify, courseController.allCourses);
// Route for retrieving all ACTIVE courses
router.get('/allActive', courseController.allActiveCourses);

// Route for retrieving all INACTIVE courses
router.get('/allNonActive', auth.verify, courseController.allInactiveCourses);

// [Routes w/ Params] (need to be separated and placed -after- codes w/o params)

// Route for retrieving details of a specified course
router.get('/:courseId', courseController.courseDetails);

// Route for updating course details
router.put('/update/:courseId', auth.verify, courseController.updateCourse);

// Route for archiving course
router.put('/archive/:courseId', auth.verify, courseController.archiveCourse);

module.exports = router;